pipeline {
    environment {
        registryCredential = 'acr-vimaan-cred1'
        registryURL = 'https://vimaan.azurecr.io'
        git_tag = """${sh(
            returnStdout: true,
            script: 'echo -n $(basename $BRANCH_NAME)'
        )}"""
        first_build = ''
        second_build = ''
    }

    options {
        office365ConnectorWebhooks([
            [name: "first", url: "https://vimaanrobotics.webhook.office.com/webhookb2/5a512041-02c9-4f1b-8c58-d47d21af5ea6@39cff306-491b-4a7c-8b36-ab27f93b4491/JenkinsCI/7b359a5b197545808d6f54a6c19a9619/9c44db76-372b-43a9-b010-2110144f976b", notifyBackToNormal: true, notifyFailure: true, notifyRepeatedFailure: true, notifySuccess: true, notifyAborted: true]
        ])
    }

    agent { label 'onprem' }

    stages {
        stage('Set Environment Variables') {
            steps {
                script {
                    // Use the variable in the array of arrays
                    env.IMAGE_DETAILS_JSON = new groovy.json.JsonBuilder(["first:${git_tag}", "second:${git_tag}"]).toPrettyString()
                }
            }
        }

        stage('Debug ENV variables') {
            environment {
                DEBUG_FLAGS = '-g'
            }
            steps {
                sh 'printenv'
            }
        }

        stage('Building first image') {
            steps {
                script {
                    def imageDetails = new groovy.json.JsonSlurper().parseText(env.IMAGE_DETAILS_JSON)
                    first_build = docker.build(imageDetails[0], '-f Dockerfile .')
                }
            }
        }

        stage('Building second image') {
            steps {
                script {
                    def imageDetails = new groovy.json.JsonSlurper().parseText(env.IMAGE_DETAILS_JSON)
                    second_build = docker.build(imageDetails[1], '-f second.Dockerfile .')
                }
            }
        }
    stage('Image Scanning') {
      when {
          expression { return !env.git_tag.contains('test') }
      }
      steps {
        script {
          def imageDetails = new groovy.json.JsonSlurper().parseText(env.IMAGE_DETAILS_JSON)

          def scriptLocation = env.IMAGE_SCANNER_SCRIPT_LOCATION

          // Join the docker images into a single string for scanning
          def joinedDockerImages = imageDetails.join(' ')

          // Run the image scanning script
          def scanResult = sh(script: "${scriptLocation} ${joinedDockerImages}", returnStatus: true)

          // Archive the generated HTML report
          archiveArtifacts artifacts: 'trivy_reports/*.html', fingerprint: true

          imageNamesToPublish = imageDetails.collect { it.replaceAll(/[:\/]/, '_') }

          imageNamesToPublish.each { imageName ->
            // Publish the report using publishHTML
            publishHTML(
                target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'trivy_reports',
                    reportFiles: "report_${imageName}.html",
                    reportName: "Trivy ${imageName} Report",
                    reportTitles: "${imageName} Report"
                ]
            )
          }

          // Send alert to Microsoft Teams if critical vulnerabilities are found
          if (scanResult != 0) {
            echo "Alerting Teams about critical vulnerabilities..."
            def vulnerableImages = readFile('trivy_reports/vulnerable_images.txt').trim().split('\n')

            office365ConnectorSend webhookUrl: 'https://vimaanrobotics.webhook.office.com/webhookb2/5a512041-02c9-4f1b-8c58-d47d21af5ea6@39cff306-491b-4a7c-8b36-ab27f93b4491/JenkinsCI/7b359a5b197545808d6f54a6c19a9619/9c44db76-372b-43a9-b010-2110144f976b',
                message: "Following images are vulnerable ${vulnerableImages.join(', ')}!",
                status: 'CRITICAL VULNERABILITIES FOUND! View Build >> Trivy Scan Report for details',
                color: '#FF8C00	'
          }
        }
      }
    }

    // stage('Push first Image') {
    //   steps{
    //     script {
    //       docker.withRegistry( registryURL, registryCredential ) {
    //         first_build.push()
    //         second_build.push()
    //       }
    //     }
    //   }
    // }

    stage('Clean up Docker images') {
        steps {
            script {
                def imageDetails = new groovy.json.JsonSlurper().parseText(env.IMAGE_DETAILS_JSON)

                imageDetails.each { image ->
                    echo "Cleaning up Docker image: ${image}"
                    sh "docker tag ${image} test_repo/${image}"
                    // Check if the image exists before attempting to remove it
                    def imageExists = sh(script: "docker images --format '{{.Repository}}:{{.Tag}}' | grep '${image}'", returnStatus: true)

                    if (imageExists == 0) {
                        sh "docker rmi ${image}"
                        sh "docker rmi test_repo/${image}"
                    } else {
                        echo "Docker image ${image} not found. Skipping cleanup."
                    }
                }
            }
        }
    }

    // stage('Remove Unused docker images') {
    //   steps{
    //     catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
    //       sh '''#!/bin/bash
    //             docker images --format '{{.Repository}}:{{.Tag}}' | grep "$first_image"
    //             docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}' | grep "$first_image")
    //             docker images --format '{{.Repository}}:{{.Tag}}' | grep "$second_image"
    //             docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}' | grep "$second_image")
    //           '''
    //     }
    //   }
    // }
    }
}




